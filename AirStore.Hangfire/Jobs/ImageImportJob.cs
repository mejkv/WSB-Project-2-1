﻿using AirShop.DataAccess.Data.Database;
using Microsoft.Extensions.Logging;
using AirShop.DataAccess.Data.Models;
using System.IO.Compression;

namespace AirStore.Hangfire.Jobs
{
	public class ImageImporterJob
	{
        private readonly string _imageFolder = "/Users/mikolajmackowiak/Documents/Project/WSB.Project.2.1/html/Produkty/AirPods pro";
        private readonly ShopDbContext _dbContext;
        private readonly ILogger<ImageImporterJob> _logger;
        public ImageImporterJob(ILogger<ImageImporterJob> logger, ShopDbContext dbContext)
        {
            _dbContext = dbContext;
            _logger = logger;
        }
        public async Task Run()
        {
            try
            {
                ConvertAndSaveImagesJob();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Błąd podczas sprawdzania stanu systemu: {ex.Message}");
            }
        }
        public void ConvertAndSaveImagesJob()
        {
            try
            {
                var imageFiles = Directory.GetFiles(_imageFolder, "*.png");

                foreach (var imageFile in imageFiles)
                {
                    var base64String = ConvertImageToBase64(imageFile);

                    SaveBase64ToDatabase(base64String);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"An error occurred while processing images: {ex.Message}");
            }
        }
        private string ConvertImageToBase64(string imagePath)
        {
            byte[] imageBytes = File.ReadAllBytes(imagePath);

            using (var outputStream = new MemoryStream())
            using (var gzipStream = new GZipStream(outputStream, CompressionMode.Compress))
            {
                gzipStream.Write(imageBytes, 0, imageBytes.Length);
            }

            return Convert.ToBase64String(imageBytes);
        }

        private void SaveBase64ToDatabase(string base64String)
        {
            var image = new Image { Base64 = base64String };
            _dbContext.Images.Add(image);
            _dbContext.SaveChanges();
        }
    }
}

