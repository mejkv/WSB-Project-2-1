﻿using AirShop.DataAccess.Data.Database;
using Microsoft.EntityFrameworkCore;

namespace AirStore.Hangfire.Jobs
{
    public class HeartbeatJob
    {
        private readonly ShopDbContext _dbContext;
        private readonly HttpClient _httpClient;

        public HeartbeatJob(ShopDbContext dbContext, HttpClient httpClient)
        {
            _dbContext = dbContext;
            _httpClient = httpClient;
        }

        public async Task Run()
        {
            try
            {
                await CheckDatabaseConnection();
                await CheckApiAvailability();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Błąd podczas sprawdzania stanu systemu: {ex.Message}");
            }
        }

        private async Task CheckDatabaseConnection()
        {
            try
            {
                await _dbContext.Database.OpenConnectionAsync();
                Console.WriteLine("Połączenie z bazą danych jest aktywne.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Błąd podczas łączenia z bazą danych: {ex.Message}");
            }
            finally
            {
                await _dbContext.Database.CloseConnectionAsync();
            }
        }

        private async Task CheckApiAvailability()
        {
            try
            {
                var response = await _httpClient.GetAsync("https://localhost:5000/api/health");
                Console.WriteLine($"{response.Content}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Błąd podczas sprawdzania dostępności API: {ex.Message}");
            }
        }
    }
}
