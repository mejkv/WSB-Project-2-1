﻿using AirShop.DataAccess.Data.Database;
using AirShop.DataAccess.Data.Entities;
using AirShop.DataAccess.Data.Models;
using AirShop.WebApiPostgre.ApiServices.Interfaces;
using AirShop.WebApiPostgre.Data.ApiExceptions;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

[Route("api/[controller]")]
[ApiController]
public class CartsController : ControllerBase
{
    private readonly ShopDbContext _context;
    private readonly IMapper _mapper;
    private readonly ILogger<CartsController> _logger;
    private readonly IUserService _userService;

    public CartsController(ShopDbContext context, IMapper mapper, ILogger<CartsController> logger, IUserService userService)
    {
        _context = context;
        _mapper = mapper;
        _logger = logger;
        _userService = userService;
    }

    [HttpGet("{userGuid}")]
    public async Task<ActionResult<CartDto>> GetCart(string userGuid)
    {
        if (!Guid.TryParse(userGuid, out var userGuidParsed))
        {
            return BadRequest("Invalid GUID format");
        }

        var cart = await _context.Carts.FirstOrDefaultAsync(c => c.Authentity == userGuidParsed);

        if (cart == null)
        {
            return NotFound();
        }

        var cartDto = _mapper.Map<CartDto>(cart);
        return Ok(cartDto);
    }

    [HttpPost]
    public async Task<ActionResult<CartDto>> PostCart(CartDto cartDto)
    {
        var cart = _mapper.Map<Cart>(cartDto);
        GetUserByGuid(cart, cartDto.UserGuid);
        _context.Carts.Add(cart);
        await _context.SaveChangesAsync();

        var createdCartDto = _mapper.Map<CartDto>(cart);
        return CreatedAtAction(nameof(GetCart), new { userGuid = createdCartDto.UserGuid }, createdCartDto);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> PutCart(int id, CartDto cartDto)
    {
        if (id != cartDto.Id)
        {
            return BadRequest();
        }

        var cart = await _context.Carts.FindAsync(id);
        if (cart == null)
        {
            return NotFound();
        }

        _mapper.Map(cartDto, cart);

        try
        {
            await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException ex)
        {
            if (!CartExists(id))
                return NotFound();

            return Content($"{ex.Message}");
        }

        return NoContent();
    }

    private bool CartExists(int id)
    {
        return _context.Carts.Any(e => e.Id == id);
    }

    private void GetUserByGuid(Cart cart, Guid userGuid)
    {
        var user = _userService.GetUserByGuid(userGuid.ToString());

        if (user is null)
            throw new UserNotExistException($"Not found user with guid: {userGuid}", new Exception());

        cart.User = user;
    }
}
