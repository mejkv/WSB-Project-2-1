﻿using AirShop.DataAccess.Data.Database;
using AirShop.DataAccess.Data.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AirShop.WebApiPostgre.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : Controller
    {
        private readonly ShopDbContext _context;
        private readonly ILogger<ImageController> _logger;

        public ImageController(ShopDbContext context, ILogger<ImageController> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Image>>> GetProducts()
        {
            return await _context.Images
                .ToListAsync();
        }
    }
}
