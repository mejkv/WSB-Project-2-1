﻿using AirShop.DataAccess.Data.Database;
using AirShop.DataAccess.Data.Entities;
using AirShop.DataAccess.Data.Models;
using AirShop.DataAccess.Data.Profiles;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AirShop.WebApiPostgre.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContractorController : ControllerBase
    {
        private readonly ShopDbContext _context;
        private readonly IMapper _mapper;

        public ContractorController(ShopDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Contractor>>> GetContractors()
        {
            return await _context.Contractor.ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Contractor>> GetContractor(int id)
        {
            var contractor = await _context.Contractor.FindAsync(id);

            if (contractor == null)
            {
                return NotFound();
            }

            return contractor;
        }

        [HttpPost]
        public async Task<ActionResult<Contractor>> PostContractor(ContractorDto contractorDto)
        {
            var contractor = MapContractor(contractorDto);
            _context.Contractor.Add(contractor);
            await _context.SaveChangesAsync();

            return CreatedAtAction(nameof(GetContractor), new { id = contractor.ContractorId }, contractor);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> PutContractor(int id, ContractorDto contractorDto)
        {
            var contractor = MapContractor(contractorDto);
            if (id != contractor.ContractorId)
            {
                return BadRequest();
            }

            _context.Entry(contractor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContractorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteContractor(int id)
        {
            var contractor = await _context.Contractor.FindAsync(id);
            if (contractor == null)
            {
                return NotFound();
            }

            contractor.ContractorStatus = DataAccess.Data.Enums.ContractorStatus.Disable;
            _context.Update(contractor);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ContractorExists(int id)
        {
            return _context.Contractor.Any(e => e.ContractorId == id);
        }

        private Contractor MapContractor(ContractorDto contractorDto)
        {
            return _mapper.Map<Contractor>(contractorDto);
        }
    }
}
