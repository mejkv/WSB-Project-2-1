using AirShop.DataAccess.Data.Database;
using AirShop.DataAccess.Data.Services.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using NLog;
using NLog.Web;
using Serilog;

namespace AirShop.WebApiPostgre
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var logger = LogManager.Setup().LoadConfigurationFromFile("nlog.config").GetCurrentClassLogger();
            try
            {
                var host = CreateHostBuilder(args).Build();

                logger.Info("Starting the application");

               /* using (var scope = host.Services.CreateScope())
                {
                    var services = scope.ServiceProvider;
                    // Perform database connection check
                    var dbChecker = services.GetRequiredService<IDatabaseChecker>();
                    if (await dbChecker.CheckConnectionAsync())
                    {
                        logger.Info("Database connection is successful");
                    }
                    else
                    {
                        logger.Error("Database connection failed");
                    }

                    var dbInitializer = services.GetRequiredService<IDatabaseInitializer>();
                    await dbInitializer.InitializeAsync();

                    logger.Info("Database migrations applied");
                }*/

                // Start Hangfire server
                //var hangfireServer = host.Services.GetRequiredService<IBackgroundJobServer>();
                //hangfireServer.Start();

                //logger.Info("Hangfire server started");

                // Run the host
                await host.RunAsync();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "An error occurred while starting the application");
                throw;
            }
            finally
            {
                LogManager.Shutdown();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .UseSerilog((hostingContext, loggerConfiguration) => loggerConfiguration
                    .WriteTo.Console())
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
                })
                .UseNLog();
    }
}