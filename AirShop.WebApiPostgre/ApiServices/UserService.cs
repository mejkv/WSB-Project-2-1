﻿using AirShop.DataAccess.Data.Database;
using AirShop.DataAccess.Data.Models;
using AirShop.DataAccess.Data.Models.Requests;
using AirShop.WebApiPostgre.ApiServices.Interfaces;
using AirShop.WebApiPostgre.Data.ApiExceptions;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace AirShop.WebApiPostgre.ApiServices
{
    public class UserService : IUserService
    {
        private readonly ShopDbContext _dbContext;
        private readonly IMapper _mapper;

        public UserService(ShopDbContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<User> AuthenticateAsync(string username, string password)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(u => u.Username == username && u.Password == CryptoHelper.Encrypt(password));
            return user is null ? GetUserNotExistMessage(username) : user;
        }

        private User GetUserNotExistMessage(string username)
        {
            throw new UserNotExistException($"User {username}: Wrong username or password", innerException: new Exception());
        }

        public async Task<User> RegisterAsync(RegisterRequestModel model)
        {
            var existingUser = await _dbContext.Users.FirstOrDefaultAsync(u => u.Username == model.Username);
            if (existingUser != null)
            {
                throw new UserNotExistException();
            }

            var newUser = new User()
            {
                Username = model.Username,
                Password = CryptoHelper.Encrypt(model.Password),
                Email = model.Email,
                UserGuid = Guid.NewGuid(),
            };

            _dbContext.Users.Add(newUser);
            await _dbContext.SaveChangesAsync();

            return _mapper.Map<User>(newUser);
        }

        public User GetUserByGuid(string userGuid)
        {
            return _dbContext.Users.FirstOrDefault(u => u.UserGuid == new Guid(userGuid));
        }
    }
}
