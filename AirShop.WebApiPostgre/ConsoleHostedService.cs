﻿using Hangfire;

namespace AirShop.WebApiPostgre
{
    public class ConsoleHostedService : IHostedService
    {
        private readonly IBackgroundJobClient _backgroundJobs;
        private readonly ILogger<ConsoleHostedService> _logger;

        public ConsoleHostedService(IBackgroundJobClient backgroundJobs, ILogger<ConsoleHostedService> logger)
        {
            _backgroundJobs = backgroundJobs;
            _logger = logger;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("AirStoreHostService is starting.");

            _backgroundJobs.Enqueue(() => Console.WriteLine("Hello, Hangfire!"));

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("AirStoreHostService is stopping.");
            return Task.CompletedTask;
        }
    }
}