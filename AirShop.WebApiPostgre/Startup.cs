﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AirShop.DataAccess.Data.Database;
using AirShop.WebApiPostgre.ApiServices;
using NLog;
using NLog.Web;
using Hangfire;
using Hangfire.MemoryStorage;
using Microsoft.OpenApi.Models;
using AirShop.DataAccess.Data.Services.Interfaces;
using AirShop.DataAccess.Data.Services;
using AirShop.WebApiPostgre.ApiServices.Interfaces;
using AirShop.DataAccess.Data.Profiles;
using Hangfire.PostgreSql;
using AirStore.Hangfire.Jobs;
using Microsoft.Extensions.Options;

namespace AirShop.WebApiPostgre
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpClient();

            services.AddLogging(loggingBuilder =>
                loggingBuilder.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace)
                              .AddNLog("nlog.config"));

            services.AddDbContext<ShopDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("DatabaseConnection")));

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IDatabaseInitializer, DatabaseInitializer>();
            services.AddScoped<IDatabaseChecker, DatabaseChecker>();
            PrepareDatabaseAsync(services);

            services.AddAutoMapper(typeof(ProductsProfile));
            services.AddAutoMapper(typeof(ReceiptProfile));
            services.AddAutoMapper(typeof(UserProfile));
            services.AddAutoMapper(typeof(CodeProfile));
            services.AddAutoMapper(typeof(ReceiptPositionProfile));
            services.AddAutoMapper(typeof(ContractorProfile));
            services.AddAutoMapper(typeof(DocumentProfile));
            services.AddAutoMapper(typeof(DocumentPositionProfile));
            services.AddAutoMapper(typeof(CartProfile));

            AddScopedJobs(services);

            services.AddRouting();
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "AirStore API", Version = "v1" });
            });

            services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseDefaultTypeSerializer()
                .UsePostgreSqlStorage(options =>
                {
                    options.UseNpgsqlConnection(Configuration.GetConnectionString("DatabaseConnection"));
                }));

            services.AddHangfireServer(options => options.WorkerCount = Environment.ProcessorCount * 5);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IRecurringJobManager recurringJobManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "v`");
                    c.RoutePrefix = string.Empty;
                });
            }
            else
            {
                app.UseSwagger();
                app.UseSwaggerUI();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "v`");
                    c.RoutePrefix = string.Empty;
                });
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseHangfireDashboard();

            recurringJobManager.AddOrUpdate<HeartbeatJob>("heartbeat-job", job => job.Run(), Cron.Minutely);
            recurringJobManager.AddOrUpdate<ImageImporterJob>("image-importer-job", job => job.Run(), Cron.Daily);
        }

        private void AddScopedJobs(IServiceCollection services)
        {
            services.AddScoped<HeartbeatJob>();
            services.AddScoped<ImageImporterJob>();
        }

        private static async Task PrepareDatabaseAsync(IServiceCollection services)
        {
            using (var scope = services.BuildServiceProvider().CreateScope())
            {
                var serviceProvider = scope.ServiceProvider;

                var logger = serviceProvider.GetRequiredService<ILogger<Startup>>();
                var dbChecker = serviceProvider.GetRequiredService<IDatabaseChecker>();
                var dbInitializer = serviceProvider.GetRequiredService<IDatabaseInitializer>();

                try
                {
                    logger.LogInformation("Checking database connection...");
                    if (await dbChecker.CheckConnectionAsync())
                    {
                        logger.LogInformation("Database connection is successful");
                    }
                    else
                    {
                        logger.LogError("Database connection failed");
                    }

                    logger.LogInformation("Initializing database...");
                    await dbInitializer.InitializeAsync();
                    logger.LogInformation("Database initialization completed.");
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "An error occurred during database initialization");
                    throw;
                }
            }
        }
    }
}
