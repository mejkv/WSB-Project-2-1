﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AirShop.WebApiPostgre.Migrations
{
    /// <inheritdoc />
    public partial class AddCartUserChecker : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            CREATE FUNCTION check_user_guid_in_cart()
            RETURNS TRIGGER AS $$
            BEGIN
                IF NOT EXISTS (
                    SELECT 1 FROM ""Users"" WHERE Guid = NEW.UserGuid
                ) THEN
                    RAISE EXCEPTION 'User GUID does not exist';
                END IF;
                RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;

            CREATE TRIGGER check_user_guid_before_insert
            BEFORE INSERT ON ""Carts""
            FOR EACH ROW
            EXECUTE FUNCTION check_user_guid_in_cart();
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
            DROP TRIGGER IF EXISTS check_user_guid_before_insert ON Carts;
            DROP FUNCTION IF EXISTS check_user_guid_in_cart;
            ");
        }
    }
}
