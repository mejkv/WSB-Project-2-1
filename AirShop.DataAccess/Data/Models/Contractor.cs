﻿using AirShop.DataAccess.Data.Enums;

namespace AirShop.DataAccess.Data.Models
{
    public class Contractor
    {
        public int ContractorId { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? Street { get; set; }
        public string City { get; set; }
        public string HouseNumber { get; set; }
        public string? ApartmentNumber { get; set; }
        public string PostalCode { get; set; }
        public string? Nip { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public ContractorStatus ContractorStatus { get; set; }
    }
}
