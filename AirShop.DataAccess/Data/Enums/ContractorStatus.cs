﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirShop.DataAccess.Data.Enums
{
    public enum ContractorStatus
    {
        Active = 0,
        Disable = 1,
    }
}
