﻿using AirShop.DataAccess.Data.Entities;
using AirShop.DataAccess.Data.Models;
using AirShop.WebApiPostgre.Data.Entities;
using AutoMapper;

namespace AirShop.DataAccess.Data.Profiles
{
    public class CartProfile : Profile
    {
        public CartProfile()
        {
            CreateMap<CartDto, Cart>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Authentity, opt => opt.MapFrom(src => src.UserGuid))
                .ForMember(dest => dest.CreatedAt, opt => opt.MapFrom(src => src.CreatedAt))
                .ForMember(dest => dest.UpdatedAt, opt => opt.MapFrom(src => src.UpdatedAt))
                .ForMember(dest => dest.User, opt => opt.Ignore());

            CreateMap<Cart, CartDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.UserGuid, opt => opt.MapFrom(src => src.Authentity))
                .ForMember(dest => dest.CreatedAt, opt => opt.MapFrom(src => src.CreatedAt))
                .ForMember(dest => dest.UpdatedAt, opt => opt.MapFrom(src => src.UpdatedAt));
        }
    }
}
