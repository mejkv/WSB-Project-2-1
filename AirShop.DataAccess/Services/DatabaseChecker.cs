﻿using AirShop.DataAccess.Data.Database;
using Microsoft.Extensions.Logging;

namespace AirShop.WebApiPostgre
{
    public class DatabaseChecker : IDatabaseChecker
    {
        private readonly ShopDbContext _context;
        private readonly ILogger<DatabaseChecker> _logger;

        public DatabaseChecker(ShopDbContext context, ILogger<DatabaseChecker> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<bool> CheckConnectionAsync()
        {
            _logger.LogInformation("Checking database connection...");
            return await _context.Database.CanConnectAsync();
        }
    }
}