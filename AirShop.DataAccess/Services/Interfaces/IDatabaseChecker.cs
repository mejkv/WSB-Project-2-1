﻿namespace AirShop.WebApiPostgre
{
    public interface IDatabaseChecker
    {
        Task<bool> CheckConnectionAsync();
    }
}