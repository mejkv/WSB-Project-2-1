﻿using AirShop.DataAccess.Data.Database;

namespace AirShop.DataAccess.Data.Services.Interfaces
{
    public interface IDatabaseInitializer
    {
        Task InitializeAsync();
    }
}