﻿using AirShop.DataAccess.Data.Database;
using AirShop.DataAccess.Data.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AirShop.DataAccess.Data.Services
{
    public class DatabaseInitializer : IDatabaseInitializer
    {
        private readonly ILogger<DatabaseInitializer> _logger;
        private readonly ShopDbContext _context;

        public DatabaseInitializer(ILogger<DatabaseInitializer> logger, ShopDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task InitializeAsync()
        {
            _logger.LogInformation("Applying migrations...");
            await _context.Database.MigrateAsync();
        }
    }
}